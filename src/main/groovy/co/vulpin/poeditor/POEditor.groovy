package co.vulpin.poeditor

import co.vulpin.poeditor.entities.Language
import co.vulpin.poeditor.entities.Term
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import okhttp3.HttpUrl
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.Request

import java.lang.reflect.Type

class POEditor {

    private static final HttpUrl BASE_URL = HttpUrl.parse("https://api.poeditor.com/v2/")

    private OkHttpClient httpClient = new OkHttpClient()
    private Gson gson = new Gson()

    private String projectId
    private String token

    POEditor(String projectId, String token) {
        this.projectId = projectId
        this.token = token
    }

    List<Language> getLanguages() {
        def url = BASE_URL.newBuilder()
            .addPathSegment("languages")
            .addPathSegment("list")
            .build()

        return get(url, "languages", new TypeToken<List<Language>>(){}.type)
    }

    List<Term> getTerms(String languageCode) {
        def url = BASE_URL.newBuilder()
            .addPathSegment("terms")
            .addPathSegment("list")
            .build()

        def body = new MultipartBody.Builder()
            .addFormDataPart("language", languageCode)

        return get(url, body, "terms", new TypeToken<List<Term>>(){}.type)
    }

    private <E> E get(HttpUrl url, MultipartBody.Builder bodyBuilder = new MultipartBody.Builder(), String subResultName, Type type) {
        def body = bodyBuilder
            .setType(MultipartBody.FORM)
            .addFormDataPart("id", projectId)
            .addFormDataPart("api_token", token)
            .build()

        def request = new Request.Builder()
            .url(url)
            .header("Content-Type", "application/x-www-form-urlencoded")
            .post(body)
            .build()

        def response = httpClient
            .newCall(request)
            .execute()

        def responseBody = response.body().string()

        def json = gson.fromJson(responseBody, JsonObject)
        def result = json.getAsJsonObject("result")
        def subResult = result.get(subResultName)
        return gson.fromJson(subResult, type)
    }

}
