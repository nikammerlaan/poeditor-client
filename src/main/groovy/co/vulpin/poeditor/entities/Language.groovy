package co.vulpin.poeditor.entities

class Language {

    String name
    String code

    long translations
    double percentage

}
