package co.vulpin.poeditor.entities

class Term {

    String term
    String context
    String plural

    Translation translation

    String reference
    String comment
    List<String> tags

}
